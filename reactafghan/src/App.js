import React from "react";
// import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar, NavbarBrand, Jumbotron, Row, Col } from "reactstrap";
import Table from "./Table";
import axios from "axios";
import { thisExpression } from "@babel/types";

class App extends React.Component {
  // Stateful Component
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataEdit: null
    };
  }
  componentDidMount() {
    this.load();
  }

  load() {
    axios
      .get("http://localhost:7100/kursus")
      .then(response => {
        console.log(response);
        this.setState({
          data: response.data
        });
      })
      .catch(e => {
        console.log(e);
        alert("Error");
      });
  }
  render() {
    return (
      <React.Fragment>
        <Navbar color="light" light>
          <NavbarBrand>Menu</NavbarBrand>
        </Navbar>
        <Row>
          <Col md="12">
            {/* <Jumbotron>
              <h1 className="display-3">Afghan</h1>
              <p className="lead">Perum. Bukit Waringin</p>
              <hr />
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Incidunt alias minus nihil numquam reiciendis hic, quisquam
                eveniet perferendis eaque, rem dignissimos repellendus! Eligendi
                aliquid voluptatibus praesentium tenetur, modi ipsam nesciunt?
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Atque
                quia ratione suscipit, recusandae officiis fuga? Vel voluptas
                veritatis magni fugiat deleniti, saepe repellendus sunt, ex
                facilis cumque, velit eveniet culpa.
              </p>
              <p className="lead">
                <button
                  className="btn btn-primary"
                  onClick={e => {
                    this.setState({
                      data: Data
                    });
                  }}
                >
                  Halo
                </button>
              </p>
            </Jumbotron> */}
            <Table nama="Afghan Eka Pangestu" data={this.state.data} />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default App;
