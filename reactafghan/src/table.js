import React from "react";
import { Table, Container, Row, Col } from "reactstrap";

function ViewTable(props) {
  return (
    <Container>
      <Row>
        <Col>
          <h2>{props.nama}</h2>
          <Table striped>
            <thead>
              <tr>
                <th>No</th>
                <th>No Pendaftaran</th>
                <th>Jenis Kursus</th>
                <th>Nama Kursus</th>
                <th>Hari</th>
                <th>Biaya Pendaftaran</th>
                <th>Biaya Fasilitas</th>
                <th>Total Pembayaran</th>
              </tr>
            </thead>

            <tbody>
              {props.data.map((x, i) => {
                return (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{x.no_pendaftaran}</td>
                    <td>{x.jenis_kursus}</td>
                    <td>{x.nama_kursus}</td>
                    <td>{x.hari}</td>
                    <td>{x.biaya_pendaftaran}</td>
                    <td>{x.biaya_fasilitas}</td>
                    <td>{x.total_pembayaran}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}

export default ViewTable;
